# osm_voirie
description de la voirie dans OpenStreetMap

Réalisé par Sébastien Poilroux, étudiant en Master à l'Université Savoie Mont-Blanc et stagiaire au CEREMA Méditerranée

[La documentation se trouve dans le wiki](https://gitlab.com/cerema-mobilite/osm_voirie/wikis/home)
